<?php

namespace App\Providers;

use App\Application;
use Illuminate\Support\ServiceProvider;
use App\Observers\ApplicationObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app()->setLocale(request()->segment(1));
        view()->share([
            'locale' => app()->getLocale(),
        ]);

        Application::observe(ApplicationObserver::class);
    }
}
