<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class CategoryService extends Model
{
    use Translatable;

    protected $translatable = ['name', 'body'];
    protected $table = 'category_services';
    protected $with = ['services'];

    public function services() {

        return $this->hasMany(Service::class, 'category_id');
    }

    public function applications()
    {
        return $this->hasMany(Application::class, 'category_service');
    }
}
