<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Application extends Model
{
    use Notifiable;

    protected $table = 'applications';
    protected $fillable = [
        "name",
        "email",
        "address",
        "phone",
        "category_service",
        "count_car",
        "message",
    ];
    public $with = ['category_services'];

    
    public function service(){
        return $this->belongsTo(Service::class, 'category_service', 'id');
    }
}
