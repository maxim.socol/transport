<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Page extends Model
{
    use Translatable;

    protected $table = 'pages';
    protected $translatable = ['title', 'meta_description', 'meta_keywords', 'excerpt', 'body', 'meta_title'];
    protected $guarded = [];
}
