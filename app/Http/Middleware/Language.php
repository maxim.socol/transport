<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Arr;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locales = config('translatable.locales');

        if (!array_key_exists($request->segment(1), config('translatable.locales'))) {
            $segments = $request->segments();
            $segments = Arr::prepend($segments, config('translatable.fallback_locale'));
            return redirect()->to(implode('/', $segments));
        }

        return $next($request);
    }
}
