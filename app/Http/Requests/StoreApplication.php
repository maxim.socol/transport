<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreApplication extends FormRequest
{
   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['min:2', 'max:25', 'required'],
            'address' => ['min:2', 'max:25', 'required'],
            'phone' => ['min:2', 'max:15', 'required'],
            'count_car' => ['min:1', 'max:2', 'required']
        ];
    }
}
