<?php

namespace App\Http\Controllers;

use App\Car;
use App\Client;
use App\Page;
use Illuminate\Http\Request;

class PricesController extends Controller
{
    protected $locale;

    public function __construct() {

        $this->locale = app()->getLocale();
    }

    public function prices() {

        $page = Page::where('slug', 'tarife')->firstOrFail();
        $page_trans = $page->translate($this->locale);

        $cars = Car::where('status', 'ON')->orderBy('order', 'asc')->get();
        $cars_trans = $cars->translate($this->locale, 'ro');

        $tarif = Page::where('slug', 'tarife')->first();
        $tarif_trans = $tarif->translate($this->locale);

        $clients = Client::where('status', 'ON')->orderBy('order', 'asc')->get();
        $clients_trans = $clients->translate($this->locale, 'ro');

        return view('our-prices', compact('page_trans', 'cars_trans', 'tarif_trans', 'clients_trans'));
    }
}
