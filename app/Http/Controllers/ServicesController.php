<?php

namespace App\Http\Controllers;

use App\CategoryService;
use App\Client;
use App\Page;
use App\Service;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    protected $locale;

    public function __construct() {

        $this->locale = app()->getLocale();
    }

    public function index() {

        $page = Page::where('slug', 'services')->firstOrFail();
        $page_trans = $page->translate($this->locale);

        $categories = CategoryService::where('status', 'ON')->get();
        $categories_trans = $categories->translate($this->locale, 'ro');

        $clients = Client::where('status', 'ON')->orderBy('order', 'asc')->get();
        $clients_trans = $clients->translate($this->locale, 'ro');

        return view('services', compact('page_trans', 'categories_trans', 'clients_trans'));
    }

    public function categories($lang, $slug = '') {

        $page = CategoryService::where('slug', $slug)->first();
        $page_trans = $page->translate($this->locale, 'ro');
        
        $services = Service::where('category_id', $page->id)->where('status', 'ON')->orderBy('order', 'asc')->get();
        $services_trans = $services->translate($this->locale, 'ro');

        $clients = Client::where('status', 'ON')->orderBy('order', 'asc')->get();
        $clients_trans = $clients->translate($this->locale, 'ro');
        
        return view('service', compact('page_trans', 'clients_trans', 'services_trans'));
    }
}
