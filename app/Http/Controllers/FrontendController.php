<?php

namespace App\Http\Controllers;

use App\Benefit;
use App\Car;
use App\CategoryService;
use App\Client;
use App\Contact;
use Illuminate\Http\Request;
use App\Page;
use App\Service;
use App\Slider;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    protected $locale;

    public function __construct() {

        $this->locale = app()->getLocale();
    }

    public function index() {

        $page = Page::where('slug', '/')->firstOrFail();
        $page_trans = $page->translate($this->locale);

        $slider = Slider::where('status', 'ON')->orderBy('order', 'asc')->get();
        $slider_trans = $slider->translate($this->locale, 'ro');

        $benefit = Benefit::where('status', 'ON')->orderBy('order', 'asc')->get();
        $benefit_trans = $benefit->translate($this->locale, 'ro');

        $categories = CategoryService::where('status', 'ON')->orderBy('order', 'asc')->get();
        $categories_trans = $categories->translate($this->locale, 'ro');

        $clients = Client::where('status', 'ON')->orderBy('order', 'asc')->get();
        $clients_trans = $clients->translate($this->locale, 'ro');

        $cars = Car::where('status', 'ON')->orderBy('order', 'asc')->get();
        $cars_trans = $cars->translate($this->locale, 'ro');

        $about_us = Page::where('slug', 'about')->first();
        $about_us_trans = $about_us->translate($this->locale);

        $services = Service::where('status', 'ON')->get();
        $services_trans = $services->translate($this->locale, 'ro');

        return view('index', compact('page_trans', 'slider_trans', 'benefit_trans', 'categories_trans', 'clients_trans', 
        'cars_trans', 'about_us_trans', 'services_trans', 'services'));
    }

    public function about() {

        $page = Page::where('slug', 'about')->firstOrFail();
        $page_trans = $page->translate($this->locale);

        $about_us = Page::where('slug', 'about')->first();
        $about_us_trans = $about_us->translate($this->locale);

        $clients = Client::where('status', 'ON')->orderBy('order', 'asc')->get();
        $clients_trans = $clients->translate($this->locale, 'ro');

        $benefit = Benefit::where('status', 'ON')->orderBy('order', 'asc')->get();
        $benefit_trans = $benefit->translate($this->locale, 'ro');

        return view('about', compact('page_trans', 'about_us_trans', 'clients_trans', 'benefit_trans'));
    }

    public function contacts() {

        $page = Page::where('slug', 'contact')->firstOrFail();
        $page_trans = $page->translate($this->locale);

        return view('contacts', compact('page_trans'));
    }

    public function thanks() {

        $page = Page::where('slug', 'thanks')->firstOrFail();
        $page_trans = $page->translate($this->locale);

        return view('components.thanks', compact('page_trans'));
    }

    public function ajaxSendContacts(Request $request)
    {
        if (!$request->ajax())
            abort('404');

        $contacts = new Contact();

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($contacts->create($request->all())) {
            Mail::send(
                'feedback.contact',
                array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'subject' => $request->get('subject'),
                    'user_message' => $request->get('message'),
                ),
                function ($message) {
                    $message->from('support@transportteam.md');
                    $message->to('contact.transportteam@gmail.com', 'Admin')->subject('Feedback');
                }
            );
            return response(['message' => 'Solicitarea de contact a fost trimisă cu succes', 'status' => 'success']);
        }
        return response(['message' => 'Error, something went wrong', 'status' => 'error']);
    }

}
