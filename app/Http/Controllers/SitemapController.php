<?php

namespace App\Http\Controllers;

use App\CategoryService;
use App\Page;
use App\Service;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function sitemap() {

        $languages = config('translatable.locales');

        foreach ($languages as $key => $language) {
            $sitemap = Sitemap::create()
                ->add(Url::create($key . '/contact'))
                ->add(Url::create($key . '/service'))
                ->add(Url::create($key . '/about'))
                ->add(Url::create($key . '/aplica'))
                ->add(Url::create($key . '/tarife'));

            CategoryService::all()->each(function (CategoryService $newsItem) use ($sitemap, $key) {
                $sitemap->add(Url::create($key . '/categorie/' . $newsItem->slug));
            });

            $sitemap->writeToFile(public_path('sitemap_' . $key . '.xml'));
        }

        $sitemap_main = Sitemap::create()
            ->add(Url::create('/sitemap_ru.xml'))
            ->add(Url::create('/sitemap_ro.xml'))
            ->add(Url::create('/sitemap_en.xml'));

        $sitemap_main->writeToFile(public_path('sitemap.xml'));
    }
}
