<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    protected $locale;

    public function __construct() {

        $this->locale = app()->getLocale();
    }
    
    public function pages($lang = '', $slug) {

        $pages_unmodule = ['contact', '/', 'about', 'services', 'tarife', 'aplica'];

        if (in_array($slug, $pages_unmodule))
            abort('404');

        $page = Page::where('slug', $slug)->firstOrFail();
        $page_trans = $page->translate($this->locale);

        return view('components.page', compact('page', 'page_trans'));
    }
}
