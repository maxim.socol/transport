<?php

namespace App\Http\Controllers;

use App\Application;
use App\Car;
use App\Client;
use App\Page;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Requests\StoreApplication;
use App\User;

class ApplicationController extends Controller
{
    protected $locale;

    public function __construct() {

        $this->locale = app()->getLocale();
    }

    public function app() {

        $page = Page::where('slug', 'application')->firstOrFail();
        $page_trans = $page->translate($this->locale);

        $clients = Client::where('status', 'ON')->orderBy('order', 'asc')->get();
        $clients_trans = $clients->translate($this->locale, 'ro');

        $cars = Car::where('status', 'ON')->orderBy('order', 'asc')->get();
        $cars_trans = $cars->translate($this->locale, 'ro');

        $services = Service::get();
        $services_trans = $services->translate($this->locale, 'ro');

        return view('application', compact('page_trans', 'clients_trans', 'cars_trans', 'services_trans'));
    }

    public function postEvent(StoreApplication $request) {

        $validated = $request->validated();
        if(!$validated) return response()->json($validated->errors());

        $app = Application::create(array(
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'message' => $request->message,
            'category_service' => $request->category_service,
            'count_car' => $request->count_car,
        ));
        
        $app->save();

        return response(['status' => 'success', 'message' => 'Thank you for leaving a request. Your application has been sent for review!']);
    }
}
