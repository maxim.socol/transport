<?php

namespace App\Observers;
use App\Application;
use App\Notifications\AdminNotification;
use App\Notifications\TemplateEmail;
use Illuminate\Support\Facades\Notification;

class ApplicationObserver
{
    public function created(Application $application)
    {
        $application->notify(new TemplateEmail($application));
        Notification::route('mail', 'contact.transportteam@gmail.com')
            ->notify(new AdminNotification($application));
    }
}
