<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Service extends Model
{
    use Translatable;
    use Cachable;

    protected $table = 'services';
    protected $translatable = ['title', 'excerpt', 'meta_title', 'meta_description'];
    public $fillable = ['title', 'excerpt', 'meta_title', 'meta_description', 'photo'];

    public function category_services() {

        return $this->belongsTo(CategoryService::class, 'category_id');
    }
    
}
