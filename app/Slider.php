<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Slider extends Model
{
    use Translatable;

    protected $table = 'sliders';
    protected $translatable = ['title', 'excerpt', 'text_button'];
}
