<?php

use Illuminate\Database\Seeder;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Database\Eloquent\Model::unguard();
        \App\Service::truncate();
        \App\Benefit::truncate();
        \App\Car::truncate();
        \App\Slider::truncate();

        for ($i = 0; $i <= 100; $i++) {

            $faker = \Faker\Factory::create();

            $service = new \App\Service();
            $service->title = $faker->service;
            // $service->meta_title = $faker->service;
            // $service->meta_description = $faker->service;
            // $service->excerpt = $faker->service;
            // $service->photo = $faker->service;
            $service->save();
        }

        // for ($i = 0; $i <= 1000; $i++) {

        //     $faker = \Faker\Factory::create();

        //     $benefit = new \App\Benefit();
        //     $benefit->name = $faker->benefit;
        //     $benefit->excerpt = $faker->sentence();
        //     $benefit->city()->associate(rand(1, 100));
        //     $benefit->save();
        // }
    }
}
