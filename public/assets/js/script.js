    var swiper = new Swiper('.car-slider', {
        slidesPerView: 4,
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1366: {
                slidesPerView: 4,
            },
            1024: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
            },
            375: {
                slidesPerView: 1,
            },
        }
    });

    var swiper = new Swiper('.services-slider', {
        slidesPerView: 4,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1366: {
                slidesPerView: 4,
            },
            1024: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 3,
            },
            375: {
                slidesPerView: 1,
            },
        }
    });

    var swiper = new Swiper('.swiper-clients', {
        slidesPerView: 6,
        spaceBetween: 110,
        breakpoints: {
            1366: {
                slidesPerView: 6,
            },
            1024: {
                slidesPerView: 6,
                spaceBetween: 60,
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 50,
            },
            375: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
        }
    });

    var swiper = new Swiper('.slider-container', {
        effect: 'fade',
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    $(document).ready(function () {
        $('#largeModal').on("show.bs.modal", function (e) {
            $("#basicModal").html($(e.relatedTarget).data('title'));
            $("#fav-title").html($(e.relatedTarget).data('title'));
            $("#img").html($(e.relatedTarget).data('img'));
            $("#fav-max").html($(e.relatedTarget).data('max'));
            $("#volum").html($(e.relatedTarget).data('volum'));
            $("#dimensions").html($(e.relatedTarget).data('dimensions'));
            $("#min").html($(e.relatedTarget).data('min'));
            $("#price-hour").html($(e.relatedTarget).data('price'));
            $("#price-km").html($(e.relatedTarget).data('price-km'));
        });
    });

    $(document).ready(function () {
        $('#leadform_1').on('submit', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var action = $(this).attr('action');
            var data = new FormData($(this)[0]);
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: action,
                data: data,
                success: function (response) {
                    if (response) {
                        window.location.href = "thanks";
                    }
                },
                error: function (data) {
                    $('.alert-danger').text('');
                    var err = '';
                    for (error in data.responseJSON.errors) {
                        err += get_trans(error) + ' ' + data.responseJSON.errors[error] + '\n\n';
                    }
                    alert(err);
                },
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON'
            });
        })
    });

    // validation
    var trans = {
        "name": "{{ __('application.last-name') }}",
        "address": "{{ __('application.address') }}",
        // "email": "{{ __('application.email') }}",
        "phone": "{{ __('application.phone-number') }}",
        "category_service": "{{ __('application.category_service') }}",
        "count_car": "{{ __('application.count_car') }}",
        "message": "{{ __('application.message') }}",
    }

    function get_trans(error) {
        return trans[error];
    };

    let customErrorMessages = {
        "validation.required": "{{ __('application.validation_required') }}",
        "validation.accepted": "{{ __('application.validation_accepted') }}",
    }

    function getCustomErrorMessages(message) {
        return customErrorMessages[message];
    }