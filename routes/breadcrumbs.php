<?php

// Home
Breadcrumbs::for('index', function ($trail) {
    $trail->push(__('bread.home'), route('index'));
});

// --pages
// Home > Page
Breadcrumbs::for('page', function ($trail, $page_trans) {
    $trail->parent('index');
    $trail->push($page_trans->title, '/' . $page_trans->slug);
});

// --products
// Home > Services
Breadcrumbs::for('services', function ($trail) {
    $trail->parent('index');
    $trail->push(__('bread.services'), route('services', app()->getLocale()));
});

//Home > Services > [Category]
Breadcrumbs::for('categories', function ($trail, $category_service) {
    $trail->parent('services');
    $trail->push($category_service->name, route('categories', [app()->getLocale(), $category_service->slug]));
});
