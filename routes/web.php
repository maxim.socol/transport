<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('translations', 'HomeController@translation')->middleware('admin.user');
});

Route::get('switch/{locale}', 'LocaleController@switch')->name('locale.switch');

Route::prefix('{locale?}')->middleware(['localized'])->group(function () {

    Route::get('/', 'FrontendController@index')->name('index');
    
    Route::get('about', 'FrontendController@about')->name('about');
    
    Route::get('service', 'ServicesController@index')->name('services');
    Route::get('/categorie/{slug}', 'ServicesController@categories')->name('categories');
    
    Route::get('tarife', 'PricesController@prices')->name('prices');
    
    Route::get('aplica', 'ApplicationController@app')->name('application');
    
    Route::get('sitemap', 'SitemapController@sitemap')->name('sitemap');

    Route::get('thanks', 'FrontendController@thanks')->name('thanks');
    
    Route::get('contact', 'FrontendController@contacts')->name('contacts');
    Route::post('/send-contacts', 'FrontendController@ajaxSendContacts')->name('send-contacts');
});

Route::post('/application', 'ApplicationController@postEvent');