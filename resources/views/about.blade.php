@extends('layouts.app', ['page_trans'=>$page_trans])     

@section('content')

<section class="top_panel_image top_panel_bg4" style="background-image: url( {{ Voyager::image($page_trans->image) }})">
    <div class="top_panel_image_hover"></div>
    <div class="top_panel_image_header">
        <h1 class="top_panel_image_title entry-title">{{ $page_trans->title }}</h1>
        {{ Breadcrumbs::render('page', $page_trans) }}
    </div>
</section>

    <div class="page_content">
        
        <section class="fullwidth_section about_section dark_section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="sc_section">
                            <div class="sc_section_overlay">
                                <div class="sc_section_content">
                                    <div class="sc_content container">
                                        <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_3 about_item">
                                            <div class="col-sm-12 sc_column_item sc_column_item_1">
                                                <h2 class="sc_title sc_title_regular">{{ $about_us_trans['title'] }}</h2>
                                                {!! $about_us_trans['body'] !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> 

        @include('components.benefits')

        @include('components.call')

        @include('components.clients')

    </div>

@endsection


            