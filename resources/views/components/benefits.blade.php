<section class="fullwidth_section price_section ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="sc_title sc_title_regular sc_align_center sc_section">{{__('benefits.title')}}</h2>
                <div class="sc_section">
                    <div class="sc_content container">
                        <div class="columns_wrap">
                            @forelse ($benefit_trans as $benefit)    
                                <div class="col-md-3 col-sm-6 col-xs-6 sc_column_item text-center">
                                    <img src="{{ Voyager::image($benefit->photo) }}" alt="{{ $benefit->title }}">
                                    <h4 class="sc_title home-benefits">{{ $benefit->title }}</h4>
                                    <p>{{ $benefit->excerpt }}</p>
                                </div>
                            @empty
                                
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>