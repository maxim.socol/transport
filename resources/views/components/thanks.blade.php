<!DOCTYPE html>
<html class="no-js">
<head>
    @include('layouts.head')
</head>
<body>

    <section class="thanks">
        <div class="container py-0">
            <div class="row">
                <div class="col-md-12 item">
                    <img src="{{asset('assets/images/logo.png')}}" alt="">
                    <h1>{{__('thanks.title')}}</h1>
                    <p>{{__('thanks.sub_title')}}</p>
                    <div class="back_page">
                        <a href="/">{{__('thanks.back_page')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>  
    
    @include('layouts.scripts')

</body>
</html>