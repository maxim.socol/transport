    <section class="fullwidth_section clients_section grey_section">
        <div class="container-fluid pb-0">
            <div class="row">
                <div class="col-sm-12">
                    <div class="sc_content container">
                        <h2 class="sc_title sc_title_regular sc_align_center">{{__('clients.title')}}</h2>
                    </div>                            
                </div>
            </div>
        </div>
        <div class="container pt-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="swiper-container swiper-clients">
                        <div class="swiper-wrapper">
                            @forelse ($clients_trans as $client)    
                            <div class="swiper-slide">
                                <div class="sc_image_wrap  alignleft">
                                    <figure class="sc_image  sc_image_shape_square">
                                        <img width="100" height="100" src="{{asset(Voyager::image($client->photo))}}" alt="{{ $client->name }}">
                                    </figure>
                                </div>
                            </div>
                            @empty
                                
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>  