<section class="fullwidth_section">
    <div class="container-fluid" style="padding-top: 0;">
        <div class="row">
            <div class="col-sm-12">
                <div class="sc_content container">
                    <div class="sc_contact_form sc_contact_form_standard standard_light">
                        <div class="sc_contact_form_left">
                            <div class="sc_contact_info">
                                <h2 class="sc_contact_form_title">{{__('contact.title')}}</h2>
                            </div>
                        </div>
                        <div class="sc_contact_form_right">
                            <form data-formtype="contact" id="contact-form" method="post" action="{{ route('send-contacts') }}" class="contact-form inited">
                                {!! csrf_field() !!}
                                <div class="sc_contact_form_info">
                                    <div class="sc_contact_form_item sc_contact_form_field label_over">
                                        <label class="required" for="sc_contact_form_username">{{__('contact.nume')}}</label>
                                        <input id="sc_contact_form_username" type="text" name="name" placeholder="{{__('contact.nume')}}">
                                    </div>
                                    <div class="sc_contact_form_item sc_contact_form_field label_over">
                                        <label class="required" for="sc_contact_form_email">{{__('contact.email')}}</label>
                                        <input id="sc_contact_form_email" type="text" name="email" placeholder="{{__('contact.email')}}">
                                    </div>
                                    <div class="sc_contact_form_item sc_contact_form_field label_over">
                                        <label class="required" for="sc_contact_form_subj">{{__('contact.subject-title')}}</label>
                                        <input id="sc_contact_form_subj" type="text" name="subject" placeholder="{{__('contact.subject')}}">
                                    </div>
                                </div>
                                <div class="sc_contact_form_item sc_contact_form_message label_over">
                                    <label class="required" for="sc_contact_form_message">{{__('contact.message-title')}}</label>
                                    <textarea id="sc_contact_form_message" name="message" placeholder="{{__('contact.message')}}"></textarea>
                                </div>
                                <div class="sc_contact_form_item sc_contact_form_button">
                                    <button type="submit">
                                        {{__('contact.send')}}
                                        <span class="icon-mail-alt"></span>
                                    </button>
                                </div>
                                <div class="result sc_infobox"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 