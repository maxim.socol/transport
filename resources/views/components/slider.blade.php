{{-- <section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_main">               
    <div id="rev_slider_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner tp-simpleresponsive">
            <ul class="tp-revslider-mainul">
                @forelse ($slider_trans as $slider)
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="400" data-saveperformance="off" class="tp-revslider-slidesli">
                        <div class="slotholder" data-bgposition="center left" data-bgfit="contain">
                            <img class="tp-bgimg defaultimg" alt="{{ $slider->title }}" src="{{ Voyager::image($slider->photo) }}" data-src="{{ Voyager::image($slider->photo) }}">
                        </div>
                        <div class="tp-caption trx-big tp-fade fadeout tp-resizeme start" data-x="center" data-hoffset="0" data-y="200" data-speed="500" data-start="600" data-easing="Quad.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="400">
                            {{ $slider->title }} 
                        </div>
                        <div class="tp-caption customin ltr start">
                            <img src="{{ Voyager::image($slider->photo) }}" alt="{{ $slider->title }}"> 
                        </div>
                        <div class="tp-caption trx-no-style sfb fadeout start" data-x="center" data-hoffset="0" data-y="535" data-speed="500" data-start="1600" data-easing="Quad.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="400">
                            <a href="{{ $slider->slug }}" class="sc_button sc_button_square sc_button_bg_light">
                                <span>{{ $slider->text_button }}</span>
                            </a> 
                        </div>
                    </li>
                @empty
                    
                @endforelse
            </ul>
        </div>           
    </div>      
</section> --}}
<section id="home-slider">
    <div class="swiper-container slider-container p-0 mb-5">
        <div class="swiper-wrapper">
            @forelse ($slider_trans as $slider)
                <div class="swiper-slide" style="background-image: url({{ Voyager::image($slider->photo) }})">
                    <div class="top_panel_image_hover"></div>
                   <h1>{{ $slider->title }} </h1>
                   <a href="tel:+37360262609" class="slider_button">
                        <span>{{ $slider->text_button }}</span>
                    </a> 
                </div>
            @empty
            
            @endforelse
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</section>