<section class="fullwidth_section no_padding_top_container calculator_bg1">
    <div class="container-fluid pb-ipad-0 pb-0">
        <div class="row">
            <div class="col-sm-12">
                <div class="sc_section calculator">
                    <div class="sc_section_overlay">
                        <div class="sc_section_content" style="padding: 60px 0;">
                            <div class="sc_content container">
                                <div class="row">
                                    <div class="sc_columns sc_app">
                                        <div class="col-md-7 col-sm-7 col-xs-7-app sc_column_item">
                                            <div class="img-section-app">
                                                <img class="img-responsive" src="{{asset('assets/images/calculator.jpg')}}" alt="">
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-12 sc_column_item col-xs-5-app">
                                            <h2 class="sc_title sc_title_regular sc_align_left h2-application">{{__('application.title')}} {{__('application.title2')}}</h2>
                                            <div class="sc_section">
                                                <p>{{__('application.excerpt')}}</p>
                                            </div>
                                            <div class="sc_section">
                                                <form name="cp_calculatedfieldsf_pform_1" id="leadform_1" action="/application" method="post" data-evalequations="1" autocomplete="on" novalidate="novalidate">
                                                    <div id="fbuilder">
                                                        <div id="fieldlist_1" class="top_aligned">
                                                            <div class="pb0 pbreak">
                                                                <div class="fields">
                                                                    <label>{{__('application.name')}}</label>
                                                                    <div class="dfield">
                                                                        <input name="name" id="name" class="field number large" type="text" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="fields">
                                                                    <label>{{__('application.email')}}</label>
                                                                    <div class="dfield">
                                                                        <input name="email" id="email" class="field number large" type="email" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="fields">
                                                                    <label>{{__('application.service')}}</label>
                                                                     <select name="category_service" id="category_service" class="selectpicker form-control dfield">
                                                                        <option selected disabled>{{__('application.select-service')}}</option>
                                                                        @foreach ($services_trans as $service)
                                                                            <option value="{{$service->id}}">{{$service->title}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="fields">
                                                                    <label>{{__('application.count-car')}}</label>
                                                                    <div class="dfield">
                                                                        <input name="count_car" id="count_car" class="field number large" type="text" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="fields">
                                                                    <label>{{__('application.address')}}</label>
                                                                    <div class="dfield">
                                                                        <input name="address" id="address" class="field number large" type="text" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="fields">
                                                                    <label>{{__('application.phone')}}</label>
                                                                    <div class="dfield">
                                                                        <input name="phone" id="phone" class="field number large" type="text" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="fields">
                                                                    <label>{{__('application.message')}}</label>
                                                                    <div class="dfield">
                                                                        <textarea name="message" id="message" class="field number large" type="text" value=""></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="fields-button">
                                                                    <button type="submit" class="application-form">{{__('application.button')}}</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>                
</section>