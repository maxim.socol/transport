<section class="fullwidth_section about_section dark_section" style="background-image: url({{ Voyager::image($about_us_trans['photo']) }});background-repeat: no-repeat;
    background-size: cover;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="sc_section">
                    <div class="sc_section_overlay">
                        <div class="sc_section_content">
                            <div class="sc_content container">
                                <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_3">
                                    <div class="col-sm-12 sc_column_item sc_column_item_1">
                                        <h2 class="sc_title sc_title_regular">{{ $about_us_trans['title'] }}</h2>
                                        <div class="body-about-home">
                                            {!! $about_us_trans['body'] !!}
                                        </div>
                                        <a href="{{ route('about', [$locale]) }}" class="sc_button sc_button_square sc_button_bg_light">
                                            <span>{{__('about.more')}}</span>
                                        </a>
                                    </div>
                                    <!--<div class="col-sm-6">-->
                                    <!--    <img class="img-responsive" src="{{ Voyager::image($about_us_trans['photo']) }}" alt="{{ $about_us_trans['title'] }}">-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 