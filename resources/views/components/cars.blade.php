    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="@if(request()->segment(2) == '') @else title-table-price @endif sc_align_center">{{__('cars.title')}}</h2>
                    <div class="swiper-container car-slider">
                        <div class="swiper-wrapper">
                            @forelse ($cars_trans as $car)
                                <div class="swiper-slide">
                                    <div class="mask-slider">
                                        <h6>{{ $car->title }} {{ $car->size }} m<sup><small>3</small></sup></h6>
                                        <p>{{ $car->dimensions }}</p>
                                        <span>{{__('cars.incarcare')}} {{ $car->lading }}</span>
                                        <button type="button" class="modal-car" 
                                            data-toggle="modal" 
                                            data-id="{{ $car->id }}"
                                            data-title="{{ $car->title }}"
                                            data-img="<img src='{{ Voyager::image($car->photo) }}'>"
                                            data-max="{{__('cars.incarcare-volum')}}: {{ $car->max_kg }} {{__('cars.volum-kg')}}"
                                            data-volum="{{__('cars.volum')}}: {{ $car->size }} {{__('cars.m')}}<sup><small>3</small></sup>"
                                            data-dimensions="{{__('cars.dimensiuni')}}: {{ $car->dimensions }}"
                                            data-min="{{__('cars.comanda-minima')}}: {{ $car->min_hour }} h"
                                            data-price="{{__('cars.price-hour')}}: {{__('cars.de-la')}} {{ $car->price }} {{__('cars.lei')}}"
                                            data-price-km="{{__('cars.price-km')}}: {{__('cars.de-la')}} {{ $car->price_km }} {{__('cars.lei')}}"
                                            data-target="#largeModal">{{__('cars.detalii')}}
                                        </button>
                                    </div>
                                    <img src="{{asset(Voyager::image($car->photo))}}" alt="{{ $car->title }}">
                                    <h6>{{ $car->title }} {{ $car->size }} {{__('cars.m')}}<sup><small>3</small></sup></h6>
                                    <p><i class="icon-truck"></i>{{__('cars.de-la')}} {{ $car->price }} {{__('cars.l-h')}} - {{__('cars.minim')}} {{ $car->min_hour }} {{__('cars.h')}}</p>
                                    <span><i class="icon-truck"></i>{{__('cars.de-la')}} {{ $car->price_km }} {{__('cars.l-km')}} - {{__('cars.minim')}} {{ $car->min_km }} {{__('cars.km')}}</span>
                                </div>
                            @empty
                                
                            @endforelse
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>

        @include('components.modal')

    </section>
    
    