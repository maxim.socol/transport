<section class="page_top_wrap no_padding_container page_top_title page_top_breadcrumbs section-call">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row row-call">
                    <div class="div-call-title">
                        <h1 class="page_title">{{__('call.title')}}</h1>
                    </div>
                    <div class="breadcrumbs">
                        <a class="breadcrumbs_item home a-call" href="{{route('application', [$locale])}}">{{__('call.ostaviti')}}</a>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>