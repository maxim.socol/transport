
<div class="modal modal-car-show fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <div class="container-fluid"  style="height: 100%">
                <div class="row" style="height: 100%">
                    <div class="col-md-6 col-sm-6 image-car">
                        <div id="img"></div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-mobile-modal" style="height: 100%">
                        <div class="item-car">
                            <h2 id="fav-title"></h2>
                            <p id="fav-max"> </p>
                            <p id="volum"></p>
                            <p id="dimensions"></p>
                            <p id="min"></p>
                            <p id="price-hour"></p>
                            <p id="price-km"></p>
                            <div class="button-app-car">
                                <a href="{{ route('application', [$locale]) }}">{{__('cars.alege')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>