@forelse ($categories_trans as $category)
    <section class="fullwidth_section news_section @if($category->id == 1) grey_section else @endif price_section">
        <div class="container-fluid pb-0">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="sc_title sc_title_regular sc_align_center sc_section">{{__('services.services-title')}} {{ $category->name }}</h2>
                </div>
            </div>
        </div>
        <div class="container py-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="swiper-container services-slider">
                        <div class="swiper-wrapper">
                        @forelse ($category->services->take(4) as $service)
                            <div class="swiper-slide column_item_1 text-center">
                                <div class="post_item post_item_news sc_blogger_item services-item">
                                    <div class="post_featured">
                                        <div class="post_thumb">
                                            <img src="{{ Voyager::image($service->photo) }}" alt="{{ $service->getTranslatedAttribute('title') }}">        
                                        </div>                                                
                                    </div>
                                    <h4 class="post_title sc_title sc_blogger_title">{{ $service->getTranslatedAttribute('title') }}</h4>
                                </div>      
                            </div>  
                        @empty
                            
                        @endforelse
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>
         
        <div class="container pt-0">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="button-all-services">
                        <a href="{{ route('categories', [$locale, $category->slug]) }}">{{__('services.all-services')}}</a>
                    </div>
                </div>
            </div>
        </div>               
    </section>
@empty
    
@endforelse