@if(count($breadcrumbs))
    <div class="breadcrumbs">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <a class="breadcrumbs_item home" href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
                <span class="breadcrumbs_delimiter"></span>
            @else
                <span class="breadcrumbs_item current">{{ $breadcrumb->title }}</span>                     
            @endif
        @endforeach
    </div>
@endif
    