@extends('layouts.app')

@section('content')

    <section class="top_panel_image top_panel_bg2" style="background-image: url( {{ Voyager::image($page_trans->image) }})">
        <div class="top_panel_image_hover"></div>
        <div class="top_panel_image_header">
            <h1 class="top_panel_image_title entry-title">{{ $page_trans->title }}</h1>
            {{ Breadcrumbs::render('page', $page_trans) }}
        </div>
    </section>

    @include('components.hart')

    <div class="page_content">
        <section class="fullwidth_section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="sc_content container">
                            <div class="sc_contact_form sc_contact_form_standard standard_light">
                                <div class="sc_contact_form_left">
                                    <div class="sc_contact_info">
                                        <h2 class="sc_contact_form_title">{{__('contact.title')}}</h2>
                                        <div class="sc_contact_form_address_wrap">
                                            <div class="sc_contact_form_address_field">
                                                <span class="sc_contact_form_address_data">
                                                    {{__('contact.address')}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sc_contact_info_bottom">
                                        <div class="sc_contact_form_address_wrap">
                                            <div class="sc_contact_form_address_field">
                                                <span class="sc_contact_form_address_label">{{__('contact.tel')}}: </span>
                                                <span class="sc_contact_form_address_data">{{__('contact.number')}}</span>
                                            </div>
                                            <div class="sc_contact_form_address_field">
                                                <span class="sc_contact_form_address_label">{{__('contact.email')}}: </span>
                                                <span class="sc_contact_form_address_data">{{__('contact.mail')}}</span>
                                            </div>
                                        </div>
                                        <div class="sc_socials sc_socials_size_small  color_icons">
                                            <div class="sc_socials_item">
                                                <a href="{{__('footer.social2')}}" target="_blank" class="social_icons social_facebook icons">
                                                    <span class="icon-facebook"></span>
                                                    <span class="sc_socials_hover icon-facebook"></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sc_contact_form_right">
                                    <form id="contact-form" action="" method="POST" class="contact-form inited">
                                        @csrf
                                        <div class="sc_contact_form_info">
                                            <div class="sc_contact_form_item sc_contact_form_field label_over">
                                                <label class="required" for="sc_contact_form_username">{{__('contact.nume')}}</label>
                                                <input id="name" type="text" name="name" placeholder="{{__('contact.nume')}}">
                                            </div>
                                            <div class="sc_contact_form_item sc_contact_form_field label_over">
                                                <label class="required" for="sc_contact_form_email">{{__('contact.email')}}</label>
                                                <input id="email" type="email" name="email" placeholder="{{__('contact.mail-user')}}">
                                            </div>
                                            <div class="sc_contact_form_item sc_contact_form_field label_over">
                                                <label class="required" for="sc_contact_form_subj">{{__('contact.subject-title')}}</label>
                                                <input id="subject" type="text" name="subject" placeholder="{{__('contact.subject')}}">
                                            </div>
                                        </div>
                                        <div class="sc_contact_form_item sc_contact_form_message label_over">
                                            <label class="required" for="sc_contact_form_message">{{__('contact.message-title')}}</label>
                                            <textarea id="message" name="message" placeholder="{{__('contact.message')}}"></textarea>
                                        </div>
                                        <div class="form-group">
                                            {!! NoCaptcha::renderJs() !!}
                                            {!! NoCaptcha::display() !!}
                                            <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                                        </div>
                                        <div class="sc_contact_form_item sc_contact_form_button">
                                            <button type="submit">
                                                {{__('contact.send')}}
                                                <span class="icon-mail-alt"></span>
                                            </button>
                                        </div>
                                        <div class="result sc_infobox"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>          
    </div>

@endsection
