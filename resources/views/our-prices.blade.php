@extends('layouts.app', ['page_trans'=>$page_trans])

@section('content')

    <section class="top_panel_image top_panel_bg1" style="background-image: url( {{ Voyager::image($page_trans->image) }})">
        <div class="top_panel_image_hover"></div>
        <div class="top_panel_image_header">
            <h1 class="top_panel_image_title entry-title">{{ $page_trans->title }}</h1>
            {{ Breadcrumbs::render('page', $page_trans) }}
        </div>
    </section>

    <div class="page_content">

        <section class="fullwidth_section price_section">
            <div class="container-fluid ipad_price_section">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="container">
                            <h2 class="sc_title sc_title_regular sc_align_center title-table-price">{{__('tarif.title-one-section')}}</h2>
                            <div class="description-car-all-moldova">
                                <p>{!!__('tarif.description-one-section')!!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        @include('components.cars')

        <section class="fullwidth_section grey_section price_section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="container">
                            <h2 class="sc_title sc_title_regular sc_align_center title-table-price">{{__('tarif.title1')}}</h2>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="sc_section">
                                        <div class="sc_table aligncenter table-responsive">
                                            {!! $tarif_trans['body'] !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--<section class="fullwidth_section price_section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="container">
                            <h2 class="sc_title sc_title_regular sc_align_center title-table-price">{{__('tarif.title2')}}</h2>
                            <div class="description-car-all-moldova">
                                <p>{!! $tarif_trans['excerpt'] !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
    </div>

    {{-- <div class="pb-80-page"> --}}
        @include('components.call')
    {{-- </div> --}}

    @include('components.clients')

@endsection


            