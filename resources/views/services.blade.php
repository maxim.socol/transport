@extends('layouts.app', ['page_trans'=>$page_trans])

@section('content')

    <section class="top_panel_image top_panel_bg1" style="background-image: url( {{ Voyager::image($page_trans->image) }})">
        <div class="top_panel_image_hover"></div>
        <div class="top_panel_image_header">
            @if(Request::route()->getName() == "services")
                <h1 class="top_panel_image_title entry-title">{{ $page_trans->title }}</h1>
            @else
                <h1 class="top_panel_image_title entry-title">{{ $page_trans->name }}</h1>
            @endif
            
            @if(Request::route()->getName() == "services")
                {{ Breadcrumbs::render('services', $categories_trans) }}
            @else
                {{ Breadcrumbs::render('categories', $page_trans) }}
            @endif
        </div>
    </section>

    <div class="page_content">
        {{-- сервисы --}}
        @include('components.services')

        {{-- <div class="pb-80-page"> --}}
        @include('components.call')
        {{-- </div> --}}

        @include('components.clients')
    </div>

@endsection