@extends('layouts.app', ['page_trans'=>$page_trans])

@section('content')

    @include('components.slider')

    <div class="page_content">
        
        {{-- преимущества --}}
        <div class="benefits-home">
            @include('components.benefits')
        </div>
        
        {{-- о нас --}}
        @include('components.about') 
        
        @include('components.cars')

        {{-- сервисы --}}
        @include('components.services')

        {{-- клиенты --}}
        @include('components.clients')

        {{-- форма контактов --}}
        @include('components.application')

        {{-- карта --}}
        @include('components.hart')
    </div>
    
@endsection