@extends('layouts.app', ['page_trans'=>$page_trans])

@section('content')
    <section class="top_panel_image top_panel_bg4" style="background-image: url( {{ Voyager::image($page_trans->image) }})">
        <div class="top_panel_image_hover"></div>
        <div class="top_panel_image_header">
            <h1 class="top_panel_image_title entry-title">{{ $page_trans->title }}</h1>
            {{ Breadcrumbs::render('page', $page_trans) }}
        </div>
    </section>

    <div class="page_content">
        
        @include('components.application')

        <div class="pt-0-section">
            @include('components.cars')
        </div>

        {{-- клиенты --}}
        @include('components.clients')

    </div>

@endsection