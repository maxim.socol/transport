<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>@if(isset($page_trans)) {{ $page_trans->meta_title ?? $page_trans->name }} @endif </title>
<meta name="description" content="@isset($page_trans) {{ $page_trans->meta_description }} @endisset">  
<meta name="keywords" content="@isset($page_trans) {{ $page_trans->meta_keywords }} @endisset">
<meta name="author" content="Transportation Team">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" type="image/x-icon" href="{{asset('assets/images/icon/favicon.ico')}}">
<link rel="shortcut icon" href="" type="image/x-icon">
<link rel="apple-touch-icon" href="">
<link rel="apple-touch-icon" sizes="72x72" href="">
<link rel="apple-touch-icon" sizes="114x114" href="">
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/swiper.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" id="timeline-css-css" href="{{asset('assets/css/timeline.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="packed-css" href="{{asset('assets/css/_packed.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="rs-settings-css" href="{{asset('assets/js/vendor/revslider/css/settings.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="fontello-style-css" href="{{asset('assets/css/fontello.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="main-style-css" href="{{asset('assets/css/main_style.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="custom-style-css" href="{{asset('assets/css/custom-style.css')}}" type="text/css" media="all">
<style id="custom-style-inline-css" type="text/css"></style>
<link rel="stylesheet" id="responsive-style-css" href="{{asset('assets/css/responsive.css')}}" type="text/css" media="all">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--<link rel="stylesheet" id="customizer-style-css" href="custom_tools/css/front.customizer.css" type="text/css" media="all">-->