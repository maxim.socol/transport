<ul id="menu-categories" class="menu">
    @foreach($items as $menu_item)
        <li class="menu-item">
            <a href="/{{ app()->getLocale() }}/{{ $menu_item->link() }}">{{ $menu_item->getTranslatedAttribute('title', app()->getLocale(), 'ro') }}</a>
        </li>
    @endforeach
</ul>