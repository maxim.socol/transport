<script type="text/javascript" src="{{asset('assets/js/vendor/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/vendor/jquery-migrate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/js/vendor/jquery.timeline.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/vendor/revslider/js/jquery.themepunch.tools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/vendor/revslider/js/jquery.themepunch.revolution.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/js/_packed.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/shortcodes.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/_main.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/swiper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap-select.min.js')}}"></script>
    
<script type="text/javascript" src="{{asset('assets/js/vendor/jquery.formstyler.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/vendor/jquery.validate.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/vendor/logical.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>
<script>
    $(document).ready(function () {
        $('#contact-form').submit(function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('send-contacts', app()->getLocale()) }}",
                data: $(this).serialize(),
                success:function(response) {
                    if(response){
                        window.location.href = "thanks";
                    }
                },
                error: function (data) {

                },
            });
        });
    });
</script>