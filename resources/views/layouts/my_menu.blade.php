<header class="top_panel_wrap"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <div class="menu_main_wrap logo_left menu_show">
        <div class="container">
            <div class="logo">
                <a href="{{ route('index', [$locale]) }}">
                    <img src="{{asset('assets/images/icon/157x54.png')}}" class="logo_main" alt="">
                    <span class="logo_info"></span>
                </a>
            </div>
            <div class="menu_main">
                <nav class="menu_main_nav_area">
                    <ul id="menu_main" class="menu_main_nav">
                        @foreach($items as $menu_item)
                            <li class="menu-item @if('/'.request()->segment(2) == $menu_item['value']) current-menu-item current-menu-parent @else @endif">
                                <a href="/{{ app()->getLocale() }}{{ $menu_item->link() }}">{{ $menu_item->getTranslatedAttribute('title', app()->getLocale(), 'ro') }}</a>
                            </li>
                        @endforeach
                        <li class="menu-item menu-item-has-children">
                            @if(array_key_exists(app()->getLocale(), config('translatable.locales')))
                                <a>
                                    {{ config('translatable.locales')[app()->getLocale()] }}
                                </a>
                                <ul class="sub-menu">
                                    @foreach(config('translatable.locales') as $key => $locales)
                                        <li class="menu-item">
                                            @if($key == app()->getLocale()) @continue @endif
                                            <a href="{{ route('locale.switch', $key) }}">{{ $locales }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    </ul>
                </nav>
                <a href="#" class="menu_main_responsive_button icon-menu"></a>
            </div>
        </div>
    </div>
</header>  