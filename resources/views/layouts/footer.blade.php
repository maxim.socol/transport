<footer class="footer_wrap bg_tint_dark footer_style_dark widget_area">
    <div class="container">
        <div class="row">
            <aside class="col-md-3 col-sm-4 widget widget_socials">
                <div class="widget_inner">
                    <div class="logo">
                        <a href="index.html">
                            <img src="{{asset('assets/images/icon/157x54.png')}}" alt="">                              
                            <span class="logo_info"></span>
                        </a>
                    </div>
                    <div class="logo_descr">{{__('footer.description')}}</div>
                    <div class="sc_socials sc_socials_size_small">
                        <div class="sc_socials_item">
                            <a href="{{__('footer.social2')}}" target="_blank" class="social_icons social_facebook icons">
                                <span class="icon-facebook"></span>
                                <span class="sc_socials_hover icon-facebook"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </aside>
            <aside class="col-md-5 col-sm-4 widget widget_text">
                <h5 class="widget_title">{{__('footer.contact')}}</h5>          
                <div class="textwidget">
                    <div class="sc_section">
                        <a href="#">
                            <b>{{__('footer.site-title')}}</b>
                        </a>
                        <br>
                        {{__('footer.address')}}
                    </div>
                    <div class="sc_section item-footer">
                        <span>{{__('footer.tel')}}:</span> {{__('footer.number')}}
                        <br><span>{{__('footer.email')}}:</span> {{__('footer.mail')}}
                    </div>
                </div>
            </aside>
            <aside class="col-md-3 col-sm-4 widget widget_nav_menu">
                <h5 class="widget_title">{{__('footer.pages')}}</h5>
                <div class="menu-categories-container">
                    {!! menu('footer', 'layouts.menu_footer')!!}
                </div>
            </aside>
        </div>
    </div>
</footer>