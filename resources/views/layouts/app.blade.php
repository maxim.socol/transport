<!DOCTYPE html>
<html class="no-js">
<head>
    @include('layouts.head')
</head>

<body class="home page body_style_fullscreen body_filled article_style_stretch top_panel_opacity_transparent top_panel_over user_menu_hide sidebar_hide">
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>

            <div class="prev-menu">
                <div class="container py-0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="item">
                                <a href="tel:{{__('home.number-menu')}}"><i class="icon-phone"></i>{{__('home.number-menu')}}</a>
                                <a href="tel:{{__('home.number-menu-2')}}"><i class="icon-phone"></i>{{__('home.number-menu-2')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {!! menu('main', 'layouts.my_menu')!!}

            @yield('content') 

            @include('layouts.footer') 
        </div>
    </div>

    <div class="preloader">
        <div class="preloader_image"></div>
    </div>

    <div class="call_port">
        <a href="tel:{{__('home.number-menu')}}"><i class="icon-phone"></i></a>
    </div>

    @include('layouts.scripts')

</body>
</html>
