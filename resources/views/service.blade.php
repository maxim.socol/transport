@extends('layouts.app', ['page_trans'=>$page_trans])

@section('content')

    <section class="top_panel_image top_panel_bg1" style="background-image: url({{ Voyager::image($page_trans->photo_breadcrumbs) }});">
        <div class="top_panel_image_hover"></div>
        <div class="top_panel_image_header">
            <h1 class="top_panel_image_title entry-title">{{ $page_trans->name }}</h1>
            {{ Breadcrumbs::render('categories', $page_trans) }}
        </div>
    </section>

    <div class="page_content container">
        <section class="fullwidth_section price_section">
            <div class="container-fluid" style="padding-bottom: 0;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="container">
                            <h2 class="sc_title sc_title_regular sc_align_center sc_section">{{__('services.services-title')}} {{ $page_trans->name }}</h2>
                            <div class="description-car-all-moldova">
                                {!! $page_trans->body !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="fullwidth_section news_section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="sc_section">
                            <div class="sc_content container">
                                <div class="sc_blogger layout_news template_news sc_blogger_horizontal">
                                    <div class="columns_wrap">
                                        @forelse ($services_trans as $service)    
                                            <div class="col-sm-3 column_item_1 text-center">
                                                <div class="post_item post_item_news sc_blogger_item services-item">
                                                    <div class="post_featured">
                                                        <div class="post_thumb">
                                                            <img src="{{ Voyager::image($service->photo) }}" alt="{{ $service->title }}">       
                                                        </div>
                                                    </div>                                                    
                                                    <h4 class="post_title sc_title sc_blogger_title">{{ $service->title }}</h4>
                                                </div>      
                                            </div>  
                                        @empty
                                            
                                        @endforelse 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
        </section>
    </div>
    
    {{-- форма контактов --}}
    @include('components.call')

    {{-- клиенты --}}
    @include('components.clients')

@endsection