<?php

return array (
  'title2' => 'Comanda',
  'address' => 'Adresa',
  'message' => 'Mesaj',
  'name' => 'Nume Prenume',
  'phone' => 'Telefon',
  'service' => 'Tipul de serviciu',
  'title' => 'Plasați',
  'count-car' => 'Numărul de mașini',
  'button' => 'Expediază',
  'select-service' => 'Selectează serviciul',
  'excerpt' => 'În scurt timp vom reveni cu un sunet!',
  'email' => 'Email',
);
