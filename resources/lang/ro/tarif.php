<?php

return array (
  'description-one-section' => 'Calcularea tarifelor pentru transportare este direct proporțională de cererea Dvs și de toate specificațiile și caracteristicile încărcăturii, precum ar fi dimensiunea, volumul, greutatea etc.<br>
Fiind fiecare comandă întotdeauna unică, costul acesteia este calculată cu exacticitatea de către managerii noștri, în baza unor factori și criterii care influențează prețul final (termini și urgență de livrare, distanța parcursului transportului de mărfuri, caracteristicile mărfurilor etc.). <br>
Pentru a stabili un preț și un plan corect de transportare nu ezitați să contactați specialiștii noștri.<br>
De asemenea, pentru fiecare marfă există și transportul adecvat acesteia. <br>Toate vehiculele au parametri tehnici diferiți în baza capacității de încărcare și volumului mărfii.',
  'title2' => 'Tarife prin Republica Moldova',
  'title1' => 'Tarife în raza orașului Chisinau',
  'title-one-section' => 'Calcularea tarifelor',
);
