<?php

return array (
  'title' => 'Tarife',
  'de-la' => 'de la',
  'detalii' => 'detalii',
  'h' => 'ora',
  'incarcare' => 'încărcare',
  'km' => 'km',
  'l-h' => 'lei - o oră',
  'l-km' => 'lei 1 km',
  'minim' => 'minim',
  'm' => 'm',
  'alege' => 'alege',
  'incarcare-volum' => 'Încărcare',
  'comanda-minima' => 'Comanda minimă',
  'price-hour' => 'Preț pe oră',
  'price-km' => 'Preț pe km',
  'volum-kg' => 'kg',
  'volum' => 'Volumul mașinii',
  'dimensiuni' => 'Dimensiunile mașinii',
  'lei' => 'lei',
);
