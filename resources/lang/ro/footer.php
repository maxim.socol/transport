<?php

return array (
  'email' => 'E-mail',
  'mail' => 'contact.transportteam@gmail.com',
  'site-title' => 'WeTransport',
  'address' => 'str.Nicolae Milescu Spătaru 19/2 .ap 32, Chișinău',
  'contact' => 'Contacte',
  'tel' => 'Telefon',
  'number' => '+37360262609; +37360262604',
  'pages' => 'Pagini',
  'description' => 'Ne găsiți pe:',
);
