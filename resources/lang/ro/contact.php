<?php

return array (
  'title' => 'Contactează-ne',
  'email' => 'Email',
  'message' => 'Mesaj',
  'nume' => 'Numele',
  'subject' => 'Subiect',
  'send' => 'Transmite',
  'address' => 'str.Nicolae Milescu Spătaru 19/2 .ap 32, Chișinău',
  'tel' => 'Telefon',
  'number' => '+37360262609; +37360262604.',
  'mail' => 'contact.transportteam@gmail.com',
);
