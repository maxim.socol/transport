<?php

return array (
  'title2' => 'Заявку',
  'address' => 'Адрес',
  'count-car' => 'Количество машин',
  'email' => 'Email',
  'excerpt' => 'Мы вам перезвоним в ближайшее время!',
  'message' => 'Сообщение',
  'name' => 'Имя Фамилия',
  'phone' => 'Телефон',
  'select-service' => 'Выберите услугу',
  'service' => 'Тип услуги',
  'title' => 'Подать',
  'button' => 'Отправить',
);
