<?php

return array (
  'address' => 'ул. Николай Милеску Спэтару 19/2 кв. 32, Кишинев',
  'contact' => 'Контакты',
  'description' => 'Наш адрес:',
  'email' => 'Email',
  'mail' => 'contact.transportteam@gmail.com',
  'number' => '+37360262609; +37360262604',
  'pages' => 'Страницы',
  'site-title' => 'WeTransport',
  'tel' => 'Телефон',
);
