<?php

return array (
  'title' => 'Тарифы',
  'alege' => 'выбрать',
  'de-la' => 'от',
  'h' => 'час',
  'incarcare' => 'загрузка',
  'km' => 'км',
  'l-h' => 'лей - час',
  'l-km' => 'лей 1 км',
  'm' => 'м',
  'minim' => 'минимум',
  'detalii' => 'подробнее',
  'comanda-minima' => 'Минимальный заказ',
  'dimensiuni' => 'Габариты машины',
  'lei' => 'лей',
  'price-hour' => 'Цена за час',
  'price-km' => 'Цена за км',
  'volum-kg' => 'кг',
  'volum' => 'Объем машины',
  'incarcare-volum' => 'Объем загрузки',
);
